
//This function populates the select dropdown with the Provinces of Canada
function loadProvinces() {
	var provArray = ["British Columbia", "Alberta", "Saskatchewan", "Manitoba", "Ontario", "Quebec", "Nova Socia", "New Brunswick", "Newfoundland and Labrador", "Yukon", "Northwest Territories", "Nunavut"];
    		
    document.getElementById("cboProv").innerHTML += "<option selected value=\"\" disabled hidden>- Select a Province -</option>";
	for(i = 0; i < provArray.length; i++) {
		document.getElementById("cboProv").innerHTML += "<option name=\"" + provArray[i] + "\" value=\"" + provArray[i] + "\">" + provArray[i] + "</option>";
	}
}

//This function ensures a value as been entered for all the form's fields and indicates to the user is any field still needs to be filled out
function validateForm() {
	if(document.getElementById("cboProv").value == "") {
		alert("Please select a Province from the dropdown.");
		document.getElementById("cboProv").focus();
	}
	else if(document.getElementById("txtName").value == "") {
		alert("Please enter a name in the Name field.");
		document.getElementById("txtName").focus();
	}
	else if(document.getElementById("txtEmail").value == "") {
		alert("Please enter an email in the Email field.");
		document.getElementById("txtEmail").focus();
	}
	else {
		alert("Everything is fine");
	}
}