var gamePieces = ["Rock", "Paper", "Scissors", "Dynamite", "Magnet"];
	$(document).ready(function(){
		
		$("#btnRock").click(function(e){
			executeGame("Rock");
		});

		$("#btnPaper").click(function(e){
			executeGame("Paper");
		});

		$("#btnScissors").click(function(e){
			executeGame("Scissors");
		});

		$("#btnDynamite").click(function(e){
			executeGame("Dynamite");
		});

		$("#btnMagnet").click(function(e){
			executeGame("Magnet");
		});

		$("#btnReset").click(function(e){
			location.reload();
		});

		function executeGame(playerPiece) {
			var cpuPiece = getRandomGamePiece(gamePieces.length);
			var winner = whoWins(playerPiece, cpuPiece)
			if(winner == "error")
			{
				$("#results").text("You entered an invalid game piece");
			}
			else
			{
				$("#playerPic").attr("src", playerPiece + ".png");
				$("#vs").attr("src", "vs.png");
				$("#cpuPic").attr("src", cpuPiece + ".png");
				for (i = 1; i <= 6; i++)
				{
					$("#line"+i).attr("src", "line.png");
				}
				if(winner == "Tie") {
					$("#results").attr("src", "tie.png");
				}
				else if (winner == "CPU") { 
					$("#results").attr("src", "cpu-win.png");
				}
				else { 
					$("#results").attr("src", "player-win.png");
				}
			}
			$("#btnRock").addClass("hidden");
			$("#btnPaper").addClass("hidden");
			$("#btnScissors").addClass("hidden");
			$("#btnDynamite").addClass("hidden");
			$("#btnMagnet").addClass("hidden");
			$("#btnReset").removeClass("hidden");
		}

		function getRandomGamePiece(length) {
			switch(Math.floor(Math.random() * length)) {
				case 0:
					return "Rock";
					break;
				case 1:
					return "Paper";
					break;
				case 2:
					return "Scissors";
					break;
				case 3:
					return "Dynamite";
					break;
				case 4:
					return "Magnet";
					break;
			}
		}

		function whoWins(player, cpu) {
			if (player == "Rock") {
				switch (cpu) {
					case "Rock":
						return "Tie";
						break;
					case "Paper":
						return "CPU";
						break;
					case "Scissors":
						return "Player";
						break;
					case "Dynamite":
						return "CPU";
						break;
					case "Magnet":
						return "Player";
						break;
				}
			}
			else if (player == "Paper") {
				switch (cpu) {
					case "Rock":
						return "Player";
						break;
					case "Paper":
						return "Tie";
						break;
					case "Scissors":
						return "CPU";
						break;
					case "Dynamite":
						return "CPU";
						break;
					case "Magnet":
						return "Player";
						break;
				}
			}
			else if (player == "Scissors") {
				switch (cpu) {
					case "Rock":
						return "CPU";
						break;
					case "Paper":
						return "Player";
						break;
					case "Scissors":
						return "Tie";
						break;
					case "Dynamite":
						return "Player";
						break;
					case "Magnet":
						return "CPU";
						break;
				}
			}
			else if (player == "Dynamite") {
				switch (cpu) {
					case "Rock":
						return "Player";
						break;
					case "Paper":
						return "Player";
						break;
					case "Scissors":
						return "CPU";
						break;
					case "Dynamite":
						return "Tie";
						break;
					case "Magnet":
						return "CPU";
						break;
				}
			}
			else if (player == "Magnet") {
				switch (cpu) {
					case "Rock":
						return "CPU";
						break;
					case "Paper":
						return "CPU";
						break;
					case "Scissors":
						return "Player";
						break;
					case "Dynamite":
						return "Player";
						break;
					case "Magnet":
						return "Tie";
						break;
				}
			}
			else {
				return "error";
			}
		}
	});